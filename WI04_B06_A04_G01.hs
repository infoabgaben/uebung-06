{-Gruppe 01: Ajein Loganathan, Lennart Ferlings, Sebastian Zebadua Hinrichs
Blatt 06, Abgabe: 25.11.2019
-}
-- Die folgende Zeile können Sie ignorieren (aber nicht löschen)
module P064 where

-- Umbenennung der Datentypen fuer eine lesbare Definition der Funktionssignaturen
-- in den Augaben a) - d)
type StudentName = String
type CourseName  = String
type ExamScore   = (CourseName, Float)
type StudentData = [(StudentName, [ExamScore])]

-- Die Studentennamen sind eine Liste von Tupeln bestehend aus dem Studentennamen
-- gefolgt von den Klausurergebnissen. Die Klausurergebnisse sind eine Liste von
-- Tupeln bestehend aus dem Namen der Veranstaltung und der erreichten Punktzahl.
-- Dabei enthalten die Daten doppelte Eintraege fuer denselben Studenten und die 
-- Klausurergebnisse enthalten teilweise falsche Veranstaltungsnamen oder Punkt-
-- zahlen ausserhalb des Intervalls [1.0, 5.0].
students :: StudentData
students = [
 ("Tim Berners-Lee", [("Mathe", 1.3), ("Algorithmen und Datenstrukturen", 2.0)]), 
 ("Ada Lovelace",[("Info 1", 3.0), ("Lambda-Kalkül", 1.7), ("Mustererkennung", 2.3), ("Data Mining", 2.7)]),
 ("Alan Turing", [("Mathe", 1.7), ("Betriebssysteme", 2.0), ("Lambda-Kalkül", 1.7)]),
 ("Alonzo Church", [("Info 2", 2.7), ("Eingebettete Systeme", 2.3), ("Lambda-Kalkuel", 1.0), ("Algorithmen und Datenstrukturen", 3.0)]),
 ("Bjarne Stroustrup", [("Info 1", 2.7), ("Info 2", 1.3), ("Betriebssysteme", 2.0), ("Algorithmische Topologie", 2.3)]),
 ("Bjarne Stroustrup", [("Info 1", 2.7), ("Info 2", 1.3), ("Betriebssysteme", 2.0), ("Algorithmische Topologie", 2.3)]),
 ("Donald E. Knuth", [("Mathe", 3.3), ("Info 2", 1.7), ("Lambda-Kalkül", 2.0), ("Mustererkennung", 4.0)]),
 ("Grace Hopper", [("Info 3", 1.0), ("Betriebssysteme", 2.3), ("Eingebettete Systeme", 1.7)]),
 ("Annie Easley", [("Mathe", 1.0), ("Info 2", 1.7)]),
 ("Edsger W. Dijkstra", [("Algorithmische Topologie", 3.3), ("Algorithmen und Datenstrukturen", 2.7), ("Eingebettete Systeme", 4.0)]),
 ("John von Neumann", [("Mathe", 3.3), ("Algoritmische Topologie", 1.0), ("Betriebssysteme", 1.3), ("Eingebettete Systeme", 5.3)])
 ]

doubles :: StudentData
doubles = [
 ("Bjarne Stroustrup", [("Info 1", 2.7), ("Info 2", 1.3), ("Betriebssysteme", 2.0), ("Algorithmische Topologie", 2.3)]),
 ("Bjarne Stroustrup", [("Info 1", 2.7), ("Info 2", 1.3), ("Betriebssysteme", 2.0), ("Algorithmische Topologie", 2.3)])]

-- Die Konstantenfunktion courses enthaelt alle zulaessigen Veranstaltungen.
-- Sie wird fuer die Aufgaben b - d benoetigt um die Klausurergebnisse vor-
-- zuverarbeiten.
courses :: [CourseName]
courses = ["Mathe", "Info 1", "Info 2", "Algorithmen und Datenstrukturen", "Betriebssysteme", "Algorithmische Topologie", "Lambda-Kalkül", "Eingebettete Systeme", "Mustererkennung", "Data Mining"]

-- examResults kann verwendet werden um die Implementation von Aufgabe b zu 
-- testen. Dabei handelt es sich um die Klausurergebnisse eines Studenten.
-- Wobei hier mehr Veranstaltungen als in der Funktion students verwendet 
-- wurden, damit alle Randfaelle in einem Beispiel getestet werden koennen.
examResults :: [ExamScore]
examResults = [("Mathe", 3.3), ("Algoritmische Topologie", 1.0), ("Betriebsysteme", 1.3), ("Eingebettete Systeme", 5.3), ("Info 1", 1.7), ("Info 2", 1.7), ("Data Mining", 0.3)]


-- ------------------Aufgabenteil a-----------------

--Durchläuft die StudentData rekursiv und filterert Elemente die noch einmal vorkommen heraus
filterDuplicates :: StudentData -> StudentData
filterDuplicates [] = []
filterDuplicates sd = [(head sd)] ++ filterDuplicates(filter (\ x -> x /= (head sd)) (tail sd))

-- ------------------Aufgabenteil b-----------------

--Gibt die "richtigen" Ergebnisse aus einer Liste mit ExamScores wieder
filterValidEntries :: [ExamScore] -> [CourseName] -> [ExamScore]
filterValidEntries es cn = filter (\ x -> ((fst x) `elem` cn) && ((snd x) >= 1.0 && (snd x) <= 5.0)) es

-- ------------------Aufgabenteil c-----------------


calculateAverages :: StudentData -> [CourseName] -> [(StudentName, Float)]
calculateAverages sd cn = map createTupel (filterDuplicates sd)
    where createTupel x = ((fst x), ((foldr (+) 0 (map (snd) (filterValidEntries (snd x) cn))) / fromIntegral (length (filterValidEntries (snd x) cn))))

-- ------------------Aufgabenteil d-----------------

variance :: StudentData -> CourseName -> [CourseName] -> Float
variance sd course cn = (foldr (\ x -> ((getResult x) - average) ^2) 0 (filterDuplicates sd)) / getDivisor where
    average = (foldr (+) 0 (map (\ x -> getResult x) (filterDuplicates sd))) / getDivisor
    getDivisor = fromIntegral (length (filterDuplicates sd))
    getResult x = foldr (+) 0 (map (\ a -> (snd a)) (filter (\ y -> (fst y) == course) (filterValidEntries (snd x) cn)))
